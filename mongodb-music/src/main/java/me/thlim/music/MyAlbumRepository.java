package me.thlim.music;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 *
 * @author: Lim, Teck Hooi
 *
 * Date: 7/25/12
 * Time: 4:17 PM
 *
 */

@Repository
public class MyAlbumRepository
{
    @Autowired
    private MongoTemplate template;

    public List<MyAlbum> findAllMyAlbums()
    {
        return template.findAll(MyAlbum.class);
    }

    public List<MyAlbum> findTrack(int trackNo)
    {
        return template.find(new Query(where("title").is("Big Whiskey and the Groo Grux King").and("tracks.number").is(trackNo)), MyAlbum.class);
    }

    public void save(MyTrack t)
    {
        template.save(t);
    }

    public void save(MyAlbum MyAlbum)
    {
        template.save(MyAlbum);
    }

    public void deleteAll()
    {
        template.remove(new Query(), MyAlbum.class);
        template.remove(new Query(), MyTrack.class);
    }

    public MyTrack findTrackInAlbum(int trackNumber, String albumName)
    {
        MyAlbum album = template.findOne(new Query(where("title").is(albumName)), MyAlbum.class);
        return template.findOne(new Query(where("number").is(trackNumber)), MyTrack.class);
    }

    public MyTrack findTrackInAlbum(String trackName, String albumName)
    {
        MyAlbum album = template.findOne(new Query(where("title").is(albumName)), MyAlbum.class);
        return template.findOne(new Query(where("name").is(trackName)), MyTrack.class);
    }
}
