package me.thlim.music;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;


/**
 * An {@link MyAlbum} represents a collection of {@link org.springframework.data.mongodb.examples.music.Track}s.
 *
 * @author Oliver Gierke
 */
@Document
public class MyAlbum
{

    @Id
    private ObjectId id;
    private String title;
    private String artist;

    /**
     * Creates a new {@link MyAlbum} with the given title and artist.
     *
     * @param title
     * @param artist
     */
    public MyAlbum(String title, String artist)
    {

        Assert.isTrue(StringUtils.hasText(artist), "Artist name must be given!");
        Assert.isTrue(StringUtils.hasText(title), "Album title must be given!");

        this.id = new ObjectId();
        this.title = title;
        this.artist = artist;
    }


    public ObjectId getId()
    {

        return id;
    }


    public String getTitle()
    {

        return title;
    }


    public String getArtist()
    {

        return artist;
    }

    /* (non-Javadoc)
      * @see java.lang.Object#equals(java.lang.Object)
      */
    @Override
    public boolean equals(Object obj)
    {

        if (this == obj)
        {
            return true;
        }

        if (obj == null || !getClass().equals(obj.getClass()))
        {
            return false;
        }

        MyAlbum that = (MyAlbum)obj;

        return id == null ? false : this.id.equals(that.id);
    }

    /* (non-Javadoc)
      * @see java.lang.Object#hashCode()
      */
    @Override
    public int hashCode()
    {

        return 17 + (id == null ? 0 : id.hashCode());
    }
}
