package me.thlim.music;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.Assert.assertEquals;


/**
 *
 * @author: Lim, Teck Hooi
 *
 * Date: 7/25/12
 * Time: 4:17 PM
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class MyAlbumRepositoryIntegrationTest
{
    @Autowired
    private MyAlbumRepository repository;

    @Before
    public void setUp()
    {
        repository.deleteAll();

        MyAlbum album = new MyAlbum("Big Whiskey and the Groo Grux King", "Dave Matthews Band");
        repository.save(album);
        MyTrack track = new MyTrack(0, "Grux");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(1, "Shake me lika a monkey");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(2, "Funny the way it is");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(3, "Lying in the hands of God");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(4, "Why I am");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(5, "Dive in");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(6, "Spaceman");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(7, "Squirm");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(8, "Alligator pie");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(9, "Seven");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(10, "Time bomb");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(11, "My baby blue");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(12, "You and me");
        track.setAlbum(album);
        repository.save(track);

        album = new MyAlbum("The Pursuit", "Jamie Cullum");
        repository.save(album);
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(0, "Just one of those things");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(1, "I'm all over it");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(2, "Wheels");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(3, "If I ruled the world");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(4, "You and me are gone");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(5, "Don't stop the music");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(6, "Love ain't gonna let you down");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(7, "Mixtape");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(8, "I think, I love");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(9, "We run things");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(10, "Not while I am around");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(11, "Music is through");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(12, "Grand Torino");
        track.setAlbum(album);
        repository.save(track);
        track = new MyTrack(13, "Grace is gone");
        track.setAlbum(album);
        repository.save(track);
    }

    @Test
    public void verifyNumberOfAlbums()
    {
        List<MyAlbum> albums = repository.findAllMyAlbums();
        assertEquals(2, albums.size());
    }

    @Test
    public void findTrackInAlbum()
    {
        MyTrack track = repository.findTrackInAlbum("Shake me lika a monkey", "Big Whiskey and the Groo Grux King");
        assertEquals("Shake me lika a monkey", track.getName());

        track = repository.findTrackInAlbum(13, "The Pursuit");
        assertEquals("Grace is gone", track.getName());

    }
}
